<?php

require_once __DIR__.'/vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$di->set("entityManager", function () {

    $dbParams = (array) $this['config']['database'];
    $config = Setup::createAnnotationMetadataConfiguration([$this['config']['entities']['entities_path']], false);

   // $config->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());


    return EntityManager::create($dbParams, $config);
});
