<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 5/11/2016
 * Time: 11:40 πμ
 */

namespace RestApi\Entity;

/**
 * @Entity @Table(name="teacher_classes")
 * @Entity(repositoryClass="Repository\TeacherClassRepository")
 **/
class TeacherClass
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /**
     *
     * @ManyToOne(targetEntity="User", cascade={"persist"})
     * @JoinColumn(name="teacher_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $teacher;

    /**
     * @ManyToOne(targetEntity="Classes",inversedBy="class_teacher", cascade={"persist"})
     * @JoinColumn(name="class_id", referencedColumnName="id")
     */
    private $classes;

    /** @Main @Column(type="integer") @GeneratedValue **/
    protected $main;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function setUser($teacher)
    {
        $this->teacher = $teacher;
        return $this;
    }


    public function setMain($main)
    {
        $this->main = $main;
    }

    public function setClass($class)
    {
        $this->classes = $class;
        return $this;
    }

    public function getClass()
    {
        return $this->classes;
    }

    /**
     * Assign entity properties using an array
     *
     * @param array $attributes assoc array of values to assign
     * @return null
     */
    public function fromArray(array $attributes)
    {
            foreach ($attributes as $name => $value) {
                if (property_exists($this, $name)) {
                    $methodName = $this->_getSetterName($name);
                    if ($methodName) {
                        $this->{$methodName}($value);
                    } else {
                        $this->$name = $value;
                    }
                }
            }
    }

    /**
     * Get property setter method name (if exists)
     *
     * @param string $propertyName entity property name
     * @return false|string
     */
    protected function _getSetterName($propertyName)
    {
        $prefixes = array('add', 'set');

        foreach ($prefixes as $prefix) {
            $methodName = sprintf('%s%s', $prefix, ucfirst(strtolower($propertyName)));
            if (method_exists($this, $methodName)) {
                return $methodName;
            }
        }
        return false;
    }



    public function saveTeacherClass(Teacher $teacher, array $class)
    {
        
            $teacher->addTeacher($this);
            $this->setUser($teacher);

            $cls = new Classes();
            $cls->fromArray($class);
            $this->setClass($cls);
        
    }


}
