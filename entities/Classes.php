<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 5/11/2016
 * Time: 11:21 πμ
 */

namespace RestApi\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/** @Entity @Table(name="classes")
 *  @Entity(repositoryClass="Repository\ClassesRepository")
 */
class Classes
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Name @Column(type="string", length=20, nullable=true) @GeneratedValue **/
    protected $name;


    /**
     * @OneToMany(targetEntity="StudentClass", mappedBy="classes")
     */
    private $class_student;


    /**
     * @OneToMany(targetEntity="TeacherClass", mappedBy="classes")
     */
    private $class_teacher;


    /** @Name @Column(type="integer", length=11) @GeneratedValue **/
    protected $teacher_num;


    /** @Name @Column(type="integer", length=11) @GeneratedValue **/
    protected $student_num;
	
	public function __construct() {
	    
		$this->class_teacher = new ArrayCollection();
		$this->class_student= new ArrayCollection();
	
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getTeacherNum()
    {
        return $this->teacher_num;
    }

    public function setTeacherNum($teacher_num)
    {
        $this->teacher_num = $teacher_num;
        return $this;
    }

    public function getStudentNum()
    {
        return $this->student_num;
    }

    public function setStudentNum($student_num)
    {
        $this->student_num = $student_num;
        return $this;
    }
	
	public function getClassTeacher()
	{
		return $this->class_teacher;
	}
	
	public function getClassStudent()
	{
		return $this->class_student;
	}

    /**
     * Assign entity properties using an array
     *
     * @param array $attributes assoc array of values to assign
     * @return null
     */
    public function fromArray(array $attributes)
    {
            foreach ($attributes as $name => $value) {
                if (property_exists($this, $name)) {
                    $methodName = $this->_getSetterName($name);
                    if ($methodName) {
                        $this->{$methodName}($value);
                    } else {
                        $this->$name = $value;
                    }
                }
            }
    }

    /**
     * Get property setter method name (if exists)
     *
     * @param string $propertyName entity property name
     * @return false|string
     */
    protected function _getSetterName($propertyName)
    {
        $prefixes = array('add', 'set');

        foreach ($prefixes as $prefix) {
            $methodName = sprintf('%s%s', $prefix, ucfirst(strtolower($propertyName)));
            if (method_exists($this, $methodName)) {
                return $methodName;
            }
        }
        return false;
    }
}
