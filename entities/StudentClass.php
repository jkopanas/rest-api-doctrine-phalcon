<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 5/11/2016
 * Time: 11:40 πμ
 */

namespace RestApi\Entity;

/**
 * @Entity @Table(name="student_classes")
 * @Entity(repositoryClass="Repository\StudentClassRepository")
 *
 **/
class StudentClass
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /**
     *
     * @ManyToOne(targetEntity="User", cascade={"persist"})
     * @JoinColumn(name="student_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $student;


    /**
     * @ManyToOne(targetEntity="Classes", inversedBy="class_student", cascade={"persist"})
     * @JoinColumn(name="class_id", referencedColumnName="id")
     */
    private $classes;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function setUser(Student $student)
    {
        $this->student = $student;
        return $this;
    }

    public function getStudent()
    {
        return $this->student;
    }


    public function getClass()
    {
        return $this->class;
    }

    public function setClass(Classes $class)
    {
        $this->classes = $class;
		return $this;
    }


    public function saveStudentClass(Student $student, array $class)
    {
        $student->addStudent($this);
        $this->setUser($student);

        $cls = new Classes();
        $cls->fromArray($class);
        $this->setClass($cls);
    }
}
