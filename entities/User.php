<?php
namespace RestApi\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RestApi\Utilities\Validatable;
use RestApi\Utilities\ValidationCapabilities;
use RestApi\Utilities\Validation;

/**
 * @Entity @Table(name="users")
 * @ORM\Entity(repositoryClass="Repository\UserRepository")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="role", type="integer")
 * @DiscriminatorMap({"0" = "User", "1" = "Student", "2" = "Teacher"})
 **/
class User implements Validatable
{
    use Validation,ValidationCapabilities;

    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Name @Column(type="string", length=20, nullable=true) @GeneratedValue **/
    protected $name;

    /** @LastName @Column(type="string", length=20, nullable=true) @GeneratedValue **/
    protected $last_name;

    /** @Email @Column(type="string", length=60, nullable=false, unique=true) @GeneratedValue **/
    protected $email;

    /** @Password @Column(type="string", length=60, nullable=false) @GeneratedValue **/
    protected $password;

    /** @LastLogin @Column(name="last_login", type="datetime", nullable=true) */
    protected $last_login;

    /** @ClassNum @Column(name="class_num", type="integer", nullable=true) */
    protected $class_num;

    /*
     *
     * OneToMany(targetEntity="TeacherClass", mappedBy="users", cascade={"persist", "remove"})
     */
    protected $teacher_classes;

    /*
     *
     * OneToMany(targetEntity="StudentClass", mappedBy="users", cascade={"persist", "remove"})
     */
    protected $student_classes;


    public function __construct()
    {
        $this->teacher_classes = new ArrayCollection();
        $this->student_classes = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id
     *
     * @return integer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Set username
     *
     * @param  string $username
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get username
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set username
     *
     * @param  string $username
     * @return User
     */
    public function setLastName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get username
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->name;
    }
    /**
     * Set password
     *
     * @param  string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Get role
     *
     * @return string
     */
    public function getRole($em)
    {
        $cmf = $em->getMetadataFactory();
        $meta = $cmf->getMetadataFor(get_called_class());
        return $meta->discriminatorValue;
    }
    /**
     * Set role
     *
     * @param  string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }
    /**
     * Set email
     *
     * @param  string $email
     * @return User
     */
    public function setEmail($email)
    {
            $this->email = $email;
            return $this;
    }
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Get lastLogin
     *
     * @return lastLogin
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }
    /**
     * Set lastLogin
     *
     * @param  string $last_login
     * @return User
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
        return $this;
    }

    public function setClassNum($class_num)
    {
        $this->class_num = $class_num;
        return $this;
    }

    public function getClassNum()
    {
        return $this->class_num;
    }

    public function getStudent()
    {
        return $this->student_classes;
    }

    public function getTeacher()
    {
        return $this->teacher_classes;
    }

    /**
     * Add a teacher class
     *
     * @param User $user
     * @return void
     */
    public function addTeacher(TeacherClass $teacher)
    {
        $this->teacher_classes[] = $teacher;
    }

    /**
     * Add a teacher class
     *
     * @param User $user
     * @return void
     */
    public function addStudent(StudentClass $student)
    {
        $this->student_classes[] = $student;
    }

    /**
     * Assign entity properties using an array
     *
     * @param array $attributes assoc array of values to assign
     * @return null
     */
    public function fromArray(array $attributes)
    {
        if ($this->validate($attributes)) {
            foreach ($attributes as $name => $value) {
                if (property_exists($this, $name)) {
                    $methodName = $this->_getSetterName($name);
                    if ($methodName) {
                        $this->{$methodName}($value);
                    } else {
                        $this->$name = $value;
                    }
                }
            }
        } else {
            throw new \RestApi\Exceptions\HTTPException (
                'Invalid value for property',
                400,
                array(
                    'dev' => 'Invalid value for property',
                    'internalCode' => 'ErrorCode:4'
                )
            );
        }
    }

    /**
     * Get property setter method name (if exists)
     *
     * @param string $propertyName entity property name
     * @return false|string
     */
    protected function _getSetterName($propertyName)
    {
        $prefixes = array('add', 'set');

        foreach ($prefixes as $prefix) {
            $methodName = sprintf('%s%s', $prefix, ucfirst(strtolower($propertyName)));
            if (method_exists($this, $methodName)) {
                return $methodName;
            }
        }
        return false;
    }


    public function validate(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if (method_exists($this, "is".ucfirst($key))) {
                if (!$this->{"is".ucfirst($key)}($value)) {
                    return false;
                }
            }
        }
        return true;
    }
}
