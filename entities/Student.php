<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 7/11/2016
 * Time: 11:49 μμ
 */

namespace RestApi\Entity;

/**
 * @Entity
 * @Entity(repositoryClass="Repository\StudentRepository")
 */
class Student extends User
{
    protected $id;

    protected $name;

    protected $role = '1';
	
	
	

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
}
