<?php
namespace RestApi\Auth;

use Phalcon\Mvc\User\Component;
use Exception;
use InvalidArgumentException;
use RestApi\Entity\StudentClass;
use RestApi\Entity\TeacherClass;
use RuntimeException;

class Auth extends Component
{
    public $authable;
    /**
     * Options array
     *
     * @var array
     */
    protected $options;

    public function __construct($authable, array $options = [])
    {
        $this->authable = $authable;
        $this->options = $options;
    }
    /**
     * Create a new User with email (must be unique) and password.
     * An $uniqueParameters array can be passed. A check will be made if those values have been already taken.
     *
     */
    public function register(
        $email,
        $password,
        array $parameters = []
    ) {

        // Check if the email is already taken
        if ($this->authable->findOneBy(["email" => $email])) {
            throw new \RestApi\Exceptions\HTTPException(
                'There is allready this email',
                401,
                array(
                    'dev' => 'Please choose another email',
                    'internalCode' => 'ErrorCode:1'
                )
            );
        }

        // Create and store the new Entity
        $className = $this->authable->getClassName();

        $entity = new $className;
        $credentials = [
            'email' => $email,
            'password' => $this->security->hash($password)
        ];

        $data = array_merge($parameters, $credentials);
        $entity->fromArray($this->authable->prepareAttributes($data));

        return $entity;
    }
    /**
     * Confirm input entity.
     * Return true on success.
     *
     * @param  int $idEntity
     * @param  string $confirmationCode
     *
     * @throws EntityNotFoundException
     *
     * @return bool
     */
    public function confirm($idEntity, $confirmationCode)
    {
        // Retrieve the entity
        if (!$entity = $this->retrieveAuthableById($idEntity)) {
            throw new Exception('Authable entity not found');
        }
        // Check the confirmation code
        if ($entity->getConfirmationCode() != $confirmationCode) {
            throw new Exception('Confirmation code is wrong');
        }
        // Confirm the entity
        $entity->confirm();
        return true;
    }
    /**
     * Attempt login of the authable entity.
     *
     * @param  string $email
     * @param  string $password
     * @param  bool $saveSession
     * @param  bool $rememberMe
     *
     * @throws EntityBannedException
     * @throws WrongCredentialsException
     *
     * @return AuthableInterface
     */
    public function attemptLogin($email, $password, $saveSession = true)
    {
        $entity = $this->authable->findOneByEmail($email);
        if (empty($entity)) {
            throw new \RestApi\Exceptions\HTTPException(
                'Wrong email/password combination',
                401,
                array(
                    'dev' => 'Wrong email/password combination',
                    'internalCode' => 'ErrorCode:2'
                )
            );
        }
		

        if (!$this->security->checkHash($password, $entity->getPassword())) {
            throw new \RestApi\Exceptions\HTTPException(
                'Wrong email/password combination',
                401,
                array(
                    'dev' => 'Wrong email/password combination.',
                    'internalCode' => 'ErrorCode:2'
                )
            );
        }


        if ((get_class($entity) == "RestApi\Entity\Teacher" )) {
            $classes=$this->getDI()->get('entityManager')
                ->getRepository('RestApi\\Entity\\Classes')
                ->findClasses($entity->getId());
			
            foreach ($classes as $key => $value) {
                $teacher_class=new TeacherClass();
                $value['teacher'] = $entity->getId();
                $teacher_class->fromArray($value);
                $entity->addTeacher($teacher_class);
            }
        }
        if ($saveSession) {
            $this->saveSessionData($entity);
        }
        return $entity;
    }
    /**
     * Returns the current identity.
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('user_auth');
    }
    /**
     * Returns the current identity email.
     *
     * @return string
     */
    public function getEmail()
    {
        $identity = $this->session->get('user_auth');
        return $identity['email'];
    }
    /**
     * Logout the entity, i.e. removes the session data but keeps the remember me environment.
     */
    public function logout()
    {
        $this->session->remove('user_auth');
    }
    /**
     * Auth the authable model by id.
     *
     * @param  int $id
     *
     * @throws EntityBannedException
     * @throws EntityNotFoundException
     */
    public function authById($id)
    {

        $entity = $this->authable->findOneById($id);
        if ($entity == false) {
            throw new Exception('The entity does not exist');
        }
        // Save the User data into the session
        $this->saveSessionData($entity);
    }
    /**
     * Retrieve and return the authable model by id.
     * Return null if the User is not found.
     *
     * @return AuthableInterface|null
     */
    public function retrieveAuthableById($id)
    {
        $entity = $this->authable->findFirstById($id);
        if ($entity == false) {
            return null;
        }
        return $entity;
    }
    /**
     * Get the stored authenticated entity.
     * Return false if no authentication has been made.
     *
     * @throws EntityNotFoundException
     * @return AuthableInterface|bool
     */
    public function getAuth()
    {
        $identity = $this->session->get('user_auth');
        if (isset($identity['id'])) {
            if (!$user = $this->retrieveAuthableById($identity['id'])) {
                throw new Exception('The authenticated model does not exist');
            }
            return $user;
        }
        return false;
    }
    /**
     * Save the User data into the Session.
     *
     */
    protected function saveSessionData($entity)
    {
        $this->session->set('user_auth', [
            'id' => $entity->getId(),
            'email' => $entity->getEmail()
        ]);
    }
}
