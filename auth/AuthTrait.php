<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 9/11/2016
 * Time: 10:15 μμ
 */
namespace RestApi\Auth;

use RestApi\Entity\Classes;
use RestApi\Entity\StudentClass;
use RestApi\Entity\TeacherClass;
use RestApi\Auth\Auth;
use RestApi\Entity\User;

trait AuthTrait
{
    public function registerAction()
    {
		
		
        $data = [
            "email" => $this->request->getPost('email', null, ""),
            "password" => $this->request->getPost('password', null, ""),
            "name" => $this->request->getPost('name', null, ""),
            "last_name" => $this->request->getPost('name', null, ""),
            "role" => $this->request->getPost('role'),
        ];

	
        foreach ($data as $key => $value) {
            if (in_array($key, $this->requiredFields)  && empty($value)) {
                throw new \RestApi\Exceptions\HTTPException(
                    'Bad Request.Empty '.$key.' not Allowed.',
                    400,
                    array(
                        'dev' => 'Error Empty '.$key.'. Registration Did not proccess',
                        'internalCode' => 'ErrorCode:3'
                    )
                );
            }
        }

        $auth = new Auth($this->di['entityManager']->getRepository("\\RestApi\\Entity\\".ucfirst($data['role'])));
        try {
            $user = $auth->register($data['email'], $data['password'], $data);
        } catch (\Exception $e) {
            throw $e;
        }


        $classes = $this->di['entityManager']->getRepository("RestApi\Entity\Classes")->getInClass($this->request->getPost('classid'));

	
        if (!empty($classes)) {
            $user->setClassNum(count($classes));
                $this->di['entityManager']->persist($user);
                $this->di['entityManager']->flush();
            if ($user->getRole($this->di['entityManager']) == 2) {
                foreach ($classes as $class) {
                    $teacher_class = new TeacherClass();
                    $teacher_class->saveTeacherClass($user, $class);
                    $mainclass = (array)$this->request->getPost('main');
                    if (in_array($class['id'], $mainclass)) {
                        $teacher_class->setMain(1);
                    } else {
                        $teacher_class->setMain(0);
                    }
                    $this->di['entityManager']->merge($teacher_class);

                    $current_class = $this->di['entityManager']->getRepository("RestApi\\Entity\\Classes")->findOneBy(["id" => $class['id']]);
                    $current_class->setTeacherNum($current_class->getTeacherNum() + 1);
                    $this->di['entityManager']->merge($current_class);
                }
            } elseif ($user->getRole($this->di['entityManager']) == 1) {
                foreach ($classes as $class) {
                    $student_class = new StudentClass();
                    $student_class->saveStudentClass($user, $class);
                    $this->di['entityManager']->merge($student_class);

                    $current_class = $this->di['entityManager']->getRepository("RestApi\\Entity\\Classes")->findOneBy(["id" => $class['id']]); // ATest is my entitity class
                    $current_class->setStudentNum($current_class->getStudentNum()+1);
                    $this->di['entityManager']->merge($current_class);
                }
            }
            $this->di['entityManager']->flush();
        } else {
			throw new \RestApi\Exceptions\HTTPException(
                    'Classes doesnt exist registration failed',
                    400,
                    array(
                        'dev' => 'Error No Valid Classes given',
                        'internalCode' => 'ErrorCode:3'
                    )
                );
		}
        return $this->respond([[
            "id" => $user->getId(),
            "name" => $user->getName(),
            "last_name" => $user->getLastName(),
            "email" => $user->getEmail(),
            "role"  => $user->getRole($this->di['entityManager'])
        ]]);
    }


    public function loginAction()
    {
        $email = $this->request->getPost('email');

        $password = $this->request->getPost('password');

        // Retrieve Auth Service
        $auth = $this->getDI()->get('auth');

        // Perform login
        $user = $auth->attemptLogin($email, $password);
        $data = [
            "last_login" => "'".date("Y-m-d H:i:s")."'"
        ];
	
        $user = $this->getDI()->get('entityManager')->getRepository('RestApi\\Entity\\Student')->updateUser($data, $user->getId());
	
        return $this->respond(["sid" => session_id()]);
    }
}
