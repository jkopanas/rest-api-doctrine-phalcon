<?php

use Phalcon\DI\FactoryDefault as DefaultDI;
use Phalcon\Mvc\Micro\Collection;
use Phalcon\Config\Adapter\Ini as IniConfig;
use Phalcon\Loader;
use RestApi\Middlewares\AuthMiddleware;



$loader = new Loader();
$loader->registerNamespaces(array(
    'RestApi\Entity' => __DIR__ . '/entities/',
    'Repository' => __DIR__ . '/entities/Repository/',
    'RestApi\Auth' => __DIR__ . '/Auth/',
    'RestApi\Utilities' => __DIR__ . '/utilities/',
    'RestApi\Middlewares' => __DIR__ . '/middlewares/',
    'RestApi\Controllers' => __DIR__ . '/controllers/',
    'RestApi\Exceptions' => __DIR__ . '/exceptions/',
    'RestApi\Responses' => __DIR__ . '/responses/'
))->register();



$di = new DefaultDI();

$di->setShared('auth', function () {
    return new RestApi\Auth\Auth($this['entityManager']->getRepository("\RestApi\Entity\User"));
});


/*
*Return the array of collections about route
*/
$di->set('collections', function () {
    return include('./routes/routeLoader.php');
});

/**
 * 
 *$di's setShared method provides a singleton instance.
 * 
 */
$di->setShared('config', function () {
    return new IniConfig("config/config.ini");
});


$di->setShared('session', function () {
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});

/**
 *  Load Bootstrap Doctrine
 */
require_once __DIR__ ."/bootstrap.php";

/**
 * 
 *
 */
$di->setShared('requestBody', function () {
    $in = file_get_contents('php://input');
    $in = json_decode($in, false);

    // JSON body could not be parsed, throw exception
    if ($in === null) {
        throw new HTTPException(
            'Data Problem',
            409,
            array(
                'dev' => 'The JSON body sent to the server was unable to be parsed.',
                'internalCode' => 'REQ2000',
                'more' => ''
            )
        );
    }
    return $in;
});

/**
 * Out application is a Micro application
 * @var $app
 */
$app = new Phalcon\Mvc\Micro();
$app->setDI($di);



/*
* Middlewares for Authentication
*/

$app->before(
    new RestApi\Middlewares\AuthMiddleware()
   //new RestApi\Middlewares\BasicMiddleware()
);


/**
 * Mount all of the collections, which makes the routes active.
 */
foreach ($di->get('collections') as $collection) {
    $app->mount($collection);
}


/*
*
* Base route return the Existince routes
*/
$app->get('/', function () use ($app) {
    $routes = $app->getRouter()->getRoutes();
    $routeDefinitions = array('GET'=>array(), 'POST'=>array(), 'PUT'=>array(), 'PATCH'=>array(), 'DELETE'=>array(), 'HEAD'=>array(), 'OPTIONS'=>array());
    foreach ($routes as $route) {
        $method = $route->getHttpMethods();
        $routeDefinitions[$method][] = $route->getPattern();
    }
    return $routeDefinitions;
});

/**
 * After request 
 */
$app->after(function () use ($app) {
	
    // OPTIONS have no body, send the headers, exit
    if ($app->request->getMethod() == 'OPTIONS') {
        $app->response->setStatusCode('200', 'OK');
        $app->response->send();
        return;
    }

    // Respond by default as JSON
    if (!$app->request->get('type') || $app->request->get('type') == 'json') {
        // Results returned from the route's controller.  All Controllers should return an array
        $records = $app->getReturnedValue();

        $response = new \RestApi\Responses\JSONResponse();
        $response->useEnvelope(true) //this is default behavior
            ->convertSnakeCase(true) //this is also default behavior
            ->send($records);

        return;
    } elseif ($app->request->get('type') == 'csv') {
        $records = $app->getReturnedValue();
        $response = new \RestApi\Responses\CSVResponse();
        $response->useHeaderRow(true)->send($records);

        return;
    } else {
        throw new \RestApi\Exceptions\HTTPException(
            'Could not return results in specified format',
            403,
            array(
                'dev' => 'Could not understand type specified by type paramter in query string.',
                'internalCode' => 'NF1000',
                'more' => 'Type may not be implemented. Choose either "csv" or "json"'
            )
        );
    }
});

$app->notFound(function () use ($app) {
    throw new \RestApi\Exceptions\HTTPException(
        'Not Found.',
        404,
        array(
            'dev' => 'That route was not found on the server.',
            'internalCode' => 'NF1000',
            'more' => 'Check route for misspellings.'
        )
    );
});


set_exception_handler(function ($exception) use ($app) {

    if (is_a($exception, 'RestApi\\Exceptions\\HTTPException')) {
		//print_r($exception);
        $exception->send();
    }
});


$app->handle();
