<?php

namespace RestApi\Utilities;

/**
 * Validation Capabilities
 */

trait ValidationCapabilities
{
    private $validationErrors;

    /**
     * Clear Validation Errors
     * @return null
     */
    public function clearValidationErrors()
    {
        $this->validationErrors = [];
    }

    /**
     * Add Validation Error
     * @param string $source
     * @param string $code
     * @param string $description
     */
    public function addValidationError($source, $code, $description)
    {
        $this->validationErrors[] = [
            'source' => $source,
            'code' => $code,
            'description' => $description,
        ];
    }

    /**
     * Get Validation Errors
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }
}