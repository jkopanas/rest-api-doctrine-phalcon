<?php

namespace RestApi\Utilities;

/**
 * Validatable interface
 */

interface Validatable
{
    public function validate(array $array);

    public function addValidationError($source, $code, $description);

    public function getValidationErrors();

    public function clearValidationErrors();
}
