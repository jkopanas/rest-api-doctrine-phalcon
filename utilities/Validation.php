<?php

namespace RestApi\Utilities;

/**
 * Validation Capabilities
 */

trait Validation
{
    /*
     * Check url Valitidy
     */
    public function isUrl($url)
    {
            $regex = "((https?|ftp)\:\/\/)."; // SCHEME
            $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
            $regex .= "([a-z0-9-.]*)(\.([a-z]{2,3}))?"; // Host or IP
            $regex .= "(\:[0-9]{2,5})?"; // Port
            $regex .= "(\/([~a-zA-Z0-9+\$_-]\.?)+)*\/?"; // Path
            $regex .= "(\?[A-Za-z+&\$_.-][A-Za-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
            $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

            if (!preg_match("/^$regex$/", $url)) {
                return false;
            }

        return true;
    }

    /*
    *   Check valid format of Date
    */
    public function isDate($date)
    {
        if (\DateTime::createFromFormat('Y-m-d H:i', $date) === false) {
            return false;
        }
        return true;
    }


    public function isEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    /*
    *   Check if Date has Expired from NOW
     *  If argument exists check it with argument
    */
    public function isDateExpired($date, $certaindate = null)
    {
        $time = ($certaindate) ? strtotime($certaindate) : time();
        if (strtotime($date) < $time) {
            return false;
        }
        return true;
    }



}