<?php

return call_user_func(function () {
    $sessionCollection = new \Phalcon\Mvc\Micro\Collection();

    $sessionCollection
        ->setPrefix('/v1/session')
        ->setHandler('\RestApi\Controllers\AuthController')
        ->setLazy(true);

    $sessionCollection->post('/', 'loginAction');
    $sessionCollection->delete('/', 'logoutAction');

    return $sessionCollection;
});
