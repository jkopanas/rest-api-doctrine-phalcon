<?php

return call_user_func(function () {
    $teacherCollection = new \Phalcon\Mvc\Micro\Collection();

    $teacherCollection
        ->setPrefix('/v1/teachers')
        ->setHandler('\RestApi\Controllers\TeacherController')
        ->setLazy(true);

    $teacherCollection->post('/', 'registerAction');
    $teacherCollection->delete('/{id}', 'deleteAction');
	
	$teacherCollection->put('/{id}/classes', 'updateClassTeacher');
	

    return $teacherCollection;
});
