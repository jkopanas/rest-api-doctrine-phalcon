<?php

return call_user_func(function () {
    $studentCollection = new \Phalcon\Mvc\Micro\Collection();

    $studentCollection
        ->setPrefix('/v1/students')
        ->setHandler('\RestApi\Controllers\StudentController')
        ->setLazy(true);

    $studentCollection->post('/', 'registerAction');
    $studentCollection->get('/{id}/classes', 'listOfClasses');
    $studentCollection->delete('/{id}', 'deleteAction');

	$studentCollection->put('/{id}/classes', 'updateClassStudent');
	
    return $studentCollection;
});
