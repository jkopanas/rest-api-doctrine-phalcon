<?php

return call_user_func(function () {
    $classCollection = new \Phalcon\Mvc\Micro\Collection();

    $classCollection
        ->setPrefix('/v1/classes')
        ->setHandler('\RestApi\Controllers\ClassController')
        ->setLazy(true);

    $classCollection->post('/', 'registerAction');
    $classCollection->get('/', 'getClassesAction');

    $classCollection->get('/{id}/students', 'getClass');
    $classCollection->delete('/{id}', 'deleteAction');

    return $classCollection;
});
