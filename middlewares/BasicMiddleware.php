<?php
namespace RestApi\Middlewares;

use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Mvc\Micro;

use RestApi\Auth\Auth;
use RestApi\Entity\Student;
use RestApi\Entity\StudentClass;
use RestApi\Entity\TeacherClass;
use RestApi\Entity\Teacher;
use RestApi\Models\User;

/**
 * CacheMiddleware
 *
 * Caches pages to reduce processing
 */
class BasicMiddleware implements MiddlewareInterface
{
    public function call(Micro $app)
    {

        $headers = $app->request->getHeaders();
        if (strpos(strtolower($headers['Authorization']), 'basic') === 0) {
            list($email, $password) = explode(':', base64_decode(substr($headers['Authorization'], 6)));

            $auth = $app->getDI()->get('auth');
            $user = $auth->attemptLogin($email, $password, false);

            $app->di->set("user", $user);

            return true;
        }

        switch ($app->getRouter()->getRewriteUri()) {
            case '/v1/session/':
                return true;
                break;
            case '/v1/teachers/':
                return true;
                break;
            case '/v1/classes/':
                return true;
                break;
            case '/v1/students/':
                return true;
                break;
        }


        throw new \RestApi\Exceptions\HTTPException(
            'You Are not Authorized.',
            401,
            array(
                'dev' => 'Please provide Authorization.',
                'internalCode' => 'ErrorCode:1'
            )
        );

        return false;
    }
}
