<?php
namespace RestApi\Middlewares;

use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Mvc\Micro;

use RestApi\Auth\Auth;
use RestApi\Entity\Teacher;
use RestApi\Models\User;

/**
 * CacheMiddleware
 *
 * Caches pages to reduce processing
 */
class AuthMiddleware implements MiddlewareInterface
{
    public function call(Micro $app)
    {
		
		
        $auth = $app->getDI()->get('auth');
        if ($auth->getIdentity()) {
            $app->di->set(
                "user",
                function () use ($app, $auth) {
                    $ses = $auth->getIdentity();
                    return $app->getDI()->get('entityManager')->getRepository("RestApi\\Entity\\User")->findOneById($ses['id']);
                },
                true
            );
            return true;
        }

        $app->di->set(
            "user",
            function () {
                return new \RestApi\Entity\User();
            },
            true
        );
		

        switch ($app->getRouter()->getRewriteUri()) {
            case '/v1/session/':
                return true;
                break;
            case '/v1/teachers/':
                return true;
                break;
            case '/v1/classes/':
                return true;
                break;
            case '/v1/students/':
                return true;
                break;
        }

        throw new \RestApi\Exceptions\HTTPException(
            'You should Login First',
            401,
            array(
                'dev' => 'Please provide credentials.',
                'internalCode' => 'ErrorCode:1'
            )
        );

        return false;
    }
}
