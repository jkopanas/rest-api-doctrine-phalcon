<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Phalcon\Config\Adapter\Ini as IniConfig;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/entities"), $isDevMode);

$conn = parse_ini_file("config/config.ini");
// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
