<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161112164114 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
                      CREATE TABLE classes_count (
                      name VARCHAR(20) NOT NULL,
                      student_num  int(11) NOT NULL,
                      teacher_num int(11) NOT NULL,
                      class_id int UNSIGNED NOT NULL,
                      class_ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP());");


        $this->addSql("
                    CREATE TRIGGER count_teachers_ins
                    AFTER INSERT ON teacher_classes
                    FOR EACH ROW
                    INSERT INTO classes_count (name,student_num,teacher_num,class_id)
                    SELECT classes.name as name,0,1,NEW.class_id FROM classes WHERE classes.id=NEW.class_id;"
        );

        $this->addSql("
                    CREATE TRIGGER count_students_ins
                    AFTER INSERT ON student_classes
                    FOR EACH ROW
                    INSERT INTO classes_count (name,student_num,teacher_num,class_id)
                    SELECT classes.name as name,1,0,NEW.class_id FROM classes WHERE classes.id=NEW.class_id;"
        );

        $this->addSql("
                    CREATE TRIGGER count_teachers_del
                    AFTER DELETE ON teacher_classes
                    FOR EACH ROW
                    DELETE FROM classes_count WHERE classes_count.class_id = OLD.class_id AND classes_count.teacher_num = 1 limit 1;"
        );

        $this->addSql("
                    CREATE TRIGGER count_students_del
                    AFTER DELETE ON student_classes
                    FOR EACH ROW
                    DELETE FROM classes_count WHERE classes_count.class_id = OLD.class_id AND classes_count.student_num = 1 limit 1;"
        );

        $this->addSql("CREATE PROCEDURE refresh_table_classes (IN ts TIMESTAMP)
                        INSERT INTO classes
                        (
                        SELECT class_id,name,SUM(teacher_num) AS teacher_num, SUM(student_num) AS student_num
                        FROM classes_count
                        WHERE class_ts < ts
                        GROUP BY class_id
                        )
                        ON DUPLICATE KEY UPDATE teacher_num=VALUES(teacher_num)+teacher_num,student_num=VALUES(student_num)+student_num;
                        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE classes_count;");
        $this->addSql("DROP TRIGGER count_teachers_ins;");
        $this->addSql("DROP TRIGGER count_students_ins;");
        $this->addSql("DROP TRIGGER count_students_del;");
        $this->addSql("DROP TRIGGER count_teachers_del;");
        $this->addSql("DROP PROCEDURE refresh_table_classes;");
    }
}
