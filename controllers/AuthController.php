<?php
namespace RestApi\Controllers;

use RestApi\Entity\Classes;
use RestApi\Entity\TeacherClass;
use RestApi\Entity\StudentClass;
use RestApi\Entity\User;
use RestApi\Exceptions\HTTPException;
use RestApi\Auth\Auth;
use RestApi\Auth\AuthTrait;

class AuthController extends RESTController
{
    use AuthTrait;

    public function logoutAction()
    {
        // Retrieve Auth Service
		
        $auth = $this->getDI()->get('auth');
        // Perform logout
        $auth->logout();

        $this->respond(["session" => "deleted"]);
    }
}
