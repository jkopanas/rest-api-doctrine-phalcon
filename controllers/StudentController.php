<?php
namespace RestApi\Controllers;

use Doctrine\ORM\EntityManager;
use RestApi\Entity\Classes;
use RestApi\Entity\Student;
use RestApi\Entity\StudentClass;
use \RestApi\Exceptions\HTTPException;
use \RestApi\Auth\AuthTrait;

class StudentController extends RESTController
{
    use AuthTrait;

    public $limit = 10;

    public $offset = 0;

    /**
     * Sets which fields may be searched against, and which fields are allowed to be returned in
     * partial responses.
     * @var array
     */
    protected $allowedFields = array(
        'partials' => array('id', 'name', 'last_name', 'student_num', 'teacher_num', 'last_login', 'class_num')
    );

    protected $requiredFields = array(
        "name", "email", "password"
    );

    /*
    public function beforeExecuteRoute($di)
    {
        if ($this->user->getId() != $id) {
            throw new \RestApi\Exceptions\HTTPException(
                'You are not supposed to see this page',
                502,
                array(
                    'dev' => 'Tried to access anothers students profile ',
                    'internalCode' => 'ErrorCode:5'
                )
            );
        }
    }*/

    public function listOfClasses($id)
    {
        return $this->respond($this->di->get('entityManager')
                    ->getRepository('RestApi\\Entity\\StudentClass')
                    ->findStudentClassesById($id));
    }


    public function deleteAction($id)
    {

        if ($id != $this->user->getId()) {
            throw new \RestApi\Exceptions\HTTPException(
                'You tried to delete an account that is not yours',
                503,
                array(
                    'dev' => 'You tried to delete an account that is not yours',
                    'internalCode' => 'ErrorCode:5'
                )
            );
        }

        try {
			
			$classes = $this->di->get('entityManager')
                ->getRepository('RestApi\\Entity\\StudentClass')
                ->findStudentClassesById($id);
			
			
			$qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\StudentClass", "t")->where('t.student='.$id)->getQuery()->execute();

            $qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\Student", "t")->where('t.id='.$id)->getQuery()->execute();

			foreach ($classes as $class) {
                $qb = $this->di->get('entityManager')->createQueryBuilder();
                $qb->update('RestApi\\Entity\\Classes', "c")
                    ->set('c.student_num','c.student_num-1')
                    ->where('c.id='.$class['id']);
                $qb->getQuery()->execute();
            }
			
            return ["status" => "SUCCESS"];
        } catch (\Exception $e) {
            return ["status" => "ERROR"];
        }
    }
	
	public function updateClassStudent ($id) 
	{
			
			if ($this->user->getId() != $id || !($this->user instanceof Student)) {
				throw new \RestApi\Exceptions\HTTPException(
                'You tried to update an account that is not yours',
                503,
                array(
                    'dev' => 'You tried to update an account that is not yours',
                    'internalCode' => 'ErrorCode:5'
                )
            );
			}
	
			$my_classes = $this->di['entityManager']->getRepository("RestApi\Entity\StudentClass")->findStudentClassesById($this->user->getId());
	
			$my_classes = array_column($my_classes,'id');
			$classes = $this->request->getPut('classid');
	
			foreach ($classes as $class) {
                   
				//dont insert classes that already exist;
				if (in_array($class,$my_classes) == true ) {
					continue;
				}
				
				$student_class = new StudentClass();
				$student_class->saveStudentClass($this->user, ["id" => $class]);
				$this->di['entityManager']->merge($student_class);
			
				$current_class = $this->di['entityManager']->getRepository("RestApi\\Entity\\Classes")->findOneBy(["id" => $class]); // ATest is my entitity class
                $current_class->setStudentNum($current_class->getStudentNum()+1);
                $this->di['entityManager']->persist($current_class);
				
			}
				
			$this->di['entityManager']->flush();
			$this->respond([]);
			
			
	}

    public function respond($results)
    {
        if (!empty($results)) {
            $newResults = array();
            $remove = array_diff(array_keys($results[0]), (($this->isPartial) ? $this->partialFields : $this->allowedFields['partials']));

            foreach ($results as $record) {
                $newResults[] = $this->array_remove_keys($record, $remove);
            }
            $results = $newResults;
        }
        return $results;
    }

    private function array_remove_keys ($array, $keys = array())
    {
        // If array is empty or not an array at all, don't bother
        // doing anything else.
        if(empty($array) || (! is_array($array))) {
            return $array;
        }

        // At this point if $keys is not an array, we can't do anything with it.
        if(! is_array($keys)) {
            return $array;
        }

        // array_diff_key() expected an associative array.
        $assocKeys = array();
        foreach($keys as $key) {
            $assocKeys[$key] = true;
        }

        return array_diff_key($array, $assocKeys);
    }
}
