<?php
namespace RestApi\Controllers;

use Doctrine\ORM\EntityManager;
use RestApi\Entity\Classes;
use RestApi\Entity\Student;
use RestApi\Entity\StudentClass;
use RestApi\Entity\Teacher;
use RestApi\Entity\TeacherClass;
use RestApi\Entity\User;
use \RestApi\Exceptions\HTTPException;
use \RestApi\Auth\AuthTrait;

class TeacherController extends RESTController
{
    
	use AuthTrait;
    public $limit = 10;

    public $offset = 0;
    /**
     * Sets which fields are allowed
     * partial responses.
     * @var array
     */
    protected $allowedFields = array(
        'partials' => array('id', 'name', 'last_name', 'email', 'role', 'main')
    );

    protected $requiredFields = array(
        "name", "email", "password", "last_name"
    );


    public function deleteAction($id)
    {

        if ($id != $this->user->getId()) {
            throw new \RestApi\Exceptions\HTTPException(
                'You tried to delete an account that is not yours',
                503,
                array(
                    'dev' => 'You tried to delete an account that is not yours',
                    'internalCode' => 'ErrorCode:5'
                )
            );
        }

        try {
			
			$classes = $this->di->get('entityManager')
                ->getRepository('RestApi\\Entity\\TeacherClass')
                ->findTeacherClassesById($id);
			
            $qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\TeacherClass", "t")->where('t.teacher='.$id)->getQuery()->execute();

            $qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\Teacher", "t")->where('t.id='.$id)->getQuery()->execute();
			
			
			foreach ($classes as $class) {
                $qb = $this->di->get('entityManager')->createQueryBuilder();
                $qb->update('RestApi\\Entity\\Classes', "c")
                    ->set('c.teacher_num','c.teacher_num-1')
                    ->where('c.id='.$class['id']);
                $qb->getQuery()->execute();
            }

            $this->respond([]);
        } catch (\Exception $e) {

            throw new \RestApi\Exceptions\HTTPException(
                'Internal Server Error',
                500,
                array(
                    'dev' => 'Failed deleting Resource',
                    'internalCode' => 'ErrorCode:5'
                )
			);
        }
    }
	
	public function updateClassTeacher ($id) 
	{
			
			if ($this->user->getId() != $id || !($this->user instanceof Teacher)) {
				throw new \RestApi\Exceptions\HTTPException(
                'You tried to update an account that is not yours',
                503,
                array(
                    'dev' => 'You tried to update an account that is not yours',
                    'internalCode' => 'ErrorCode:5'
                )
            );
			}
	
			$my_classes = $this->di['entityManager']->getRepository("RestApi\Entity\TeacherClass")->findTeacherClassesById($this->user->getId());
	
			$my_classes = array_column($my_classes,'id');
			
		
			$classes = $this->request->getPut('classid');
	
			foreach ($classes as $class) {
                   
				   
				//dont insert classes that already exist;
				if (in_array($class,$my_classes) == true ) {
					continue;
				}
			
				$student_class = new TeacherClass();
				$student_class->saveTeacherClass($this->user, ["id" => $class]);
				
				/* Should be change in order to register by users will */
				$student_class->setMain(0);
				$this->di['entityManager']->merge($student_class);
				
				$current_class = $this->di['entityManager']->getRepository("RestApi\\Entity\\Classes")->findOneBy(["id" => $class ]); // ATest is my entitity class
                $current_class->setTeacherNum($current_class->getTeacherNum()+1);
                $this->di['entityManager']->persist($current_class);
			
			}
				
			$this->di['entityManager']->flush();
			$this->respond([]);
			
	}


    private function array_remove_keys ($array, $keys = array())
    {
        // If array is empty or not an array at all, don't bother
        // doing anything else.
        if (empty($array) || (! is_array($array))) {
            return $array;
        }

        // At this point if $keys is not an array, we can't do anything with it.
        if (! is_array($keys)) {
            return $array;
        }

        // array_diff_key() expected an associative array.
        $assocKeys = array();
        foreach ($keys as $key) {
            $assocKeys[$key] = true;
        }

        return array_diff_key($array, $assocKeys);
    }

    public function respond($results)
    {
        if (!empty($results)) {
            $newResults = array();
            $remove = array_diff(array_keys($results[0]), (($this->isPartial) ? $this->partialFields : $this->allowedFields['partials']));

            foreach ($results as $record) {
                $newResults[] = $this->array_remove_keys($record, $remove);
            }
            $results = $newResults;
        }
        return $results;
    }
}
