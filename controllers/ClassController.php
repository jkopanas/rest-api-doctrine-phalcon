<?php
namespace RestApi\Controllers;

use Doctrine\ORM\EntityManager;
use RestApi\Entity\Classes;
use RestApi\Entity\Student;
use RestApi\Entity\StudentClass;
use RestApi\Entity\Teacher;
use \RestApi\Exceptions\HTTPException;
use \RestApi\Auth\AuthTrait;

class ClassController extends RESTController
{
    use AuthTrait;

    public $limit = 10;

    public $offset = 0;

    /**
     * Sets which fields may be searched against, and which fields are allowed to be returned in
     * partial responses.
     * @var array
     */
    protected $allowedFields = array(
        'partials' => array('id', 'name', "last_name", 'classNum', 'last_login')
    );


    public function beforeExecuteRoute($di)
    {
        if ($this->user instanceof Student) {
            throw new \RestApi\Exceptions\HTTPException(
                'You are not supposed to see this page',
                403,
                array(
                    'dev' => 'Tried to access Teachers Area',
                    'internalCode' => 'ErrorCode:5'
                )
            );
        }
    }

    public function getClassesAction()
    {
        return $this->respond($this->di->get('entityManager')
                    ->getRepository('RestApi\Entity\Classes')
                    ->findBy(array(), array('id' => 'Desc'), $this->limit, $this->offset));
    }


	public function getClass($id)
    {
        $teacher_class = $this->di->get('entityManager')->getRepository('RestApi\Entity\TeacherClass')->findTeacherClassesById($this->user->getId());

		
		
        foreach ($teacher_class as $value) {
            if ($value['main'] == 1 && $id == $value['id']) {
                array_push($this->allowedFields['partials'], "email");
            }
        }

	
        return $this->respond($this->di->get('entityManager')
            ->getRepository('RestApi\Entity\Classes')
            ->findStudents(['class_id' => $id], $this->orderby, $this->direction, $this->limit, $this->offset));
    }
	
    public function deleteAction($id)
    {
		
		$class = $this->di->get('entityManager')->getRepository('RestApi\\Entity\\TeacherClass')->findTeacherMainClass($this->user->getId());
		
		if ( !empty($class) && in_array($id, $class) == true ) {	
			$qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\StudentClass", "c")->where('c.class='.$id)->getQuery()->execute();
			$qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\TeacherClass", "c")->where('c.class='.$id)->getQuery()->execute();
			
			$qb = $this->di->get('entityManager')->createQueryBuilder();
            $qb->delete("RestApi\\Entity\\Classes", "c")->where('c.id='.$id)->getQuery()->execute();
		} else {
			throw new \RestApi\Exceptions\HTTPException(
                'You have are not the main owner of this class you cannot delete this class',
                503,
                array(
                    'dev' => 'Error Delete Class',
                    'internalCode' => 'ErrorCode:7'
                )
            );
		}
    }

    public function registerAction()
    {
        $name = $this->request->getPost('name', null, "");

        if (empty($name)) {
            throw new \RestApi\Exceptions\HTTPException(
                'Bad Request.Empty Value not Allowed.',
                400,
                array(
                    'dev' => 'Error Empty Name. Registration Did not proccess',
                    'internalCode' => 'ErrorCode:3'
                )
            );
        } elseif ($this->di['entityManager']->getRepository("\\RestApi\\Entity\\Classes")->findOneBy(["name" => $name])) {
            throw new \RestApi\Exceptions\HTTPException(
                'There is Allready a class with this name. Registration failed',
                409,
                array(
                    'dev' => 'Error Same Name',
                    'internalCode' => 'ErrorCode:3'
                )
            );
        } else {
            $class = new Classes();
            $class->setName($name);
            $class->setStudentNum(0);
            $class->setTeacherNum(0);
            $this->di['entityManager']->persist($class);
            $this->di['entityManager']->flush();
        }

        return $this->respond([
            [
                "id" => $class->getId(),
                "name" => $class->getName()
            ]
        ]);
    }


    public function respond($results)
    {
        if (!empty($results)) {
            $newResults = array();
            $remove = array_diff(array_keys($results[0]), (($this->isPartial) ? $this->partialFields : $this->allowedFields['partials']));

            foreach ($results as $record) {
                $newResults[] = $this->array_remove_keys($record, $remove);
            }
            $results = $newResults;
        }
        return $results;
    }

    private function array_remove_keys ($array, $keys = array())
    {
        // If array is empty or not an array at all, don't bother
        // doing anything else.
        if (empty($array) || (!is_array($array))) {
            return $array;
        }

        // At this point if $keys is not an array, we can't do anything with it.
        if (!is_array($keys)) {
            return $array;
        }

        // array_diff_key() expected an associative array.
        $assocKeys = array();
        foreach ($keys as $key) {
            $assocKeys[$key] = true;
        }

        return array_diff_key($array, $assocKeys);
    }
}
